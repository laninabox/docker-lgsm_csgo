FROM debian:latest

MAINTAINER laninabox <info@utopit.be>

RUN dpkg --add-architecture i386 && \
   apt-get update -y && \
   apt-get install -y mailutils postfix curl wget file \
   gzip bzip2 bsdmainutils python util-linux tmux \
   lib32gcc1 libstdc++6 libstdc++6:i386 vim

RUN useradd -m -s /bin/bash csgoserver

WORKDIR /home/csgoserver
ADD csgoserver . 
RUN chown -R csgoserver:csgoserver /home/csgoserver

USER csgoserver

RUN ./csgoserver auto-install
RUN ./csgoserver validate

VOLUME /home/csgoserver/csgoserver

CMD ["./csgoserver","debug"]
