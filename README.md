# docker-lgsm_csgo

Docker container that runs a [Linux Game Server Manager](https://gameservermanagers.com/) instance for Counter-Strike: Global Offensive.

## Usage

To just start the server:

    docker run --rm laninabox/lgsm-csgo

To run give your own parameters to the script:

    cd /tmp
    wget https://gameservermanagers.com/dl/csgoserver
    vim csgoserver
    docker run --rm -v /tmp/csgoserver:csgoserver laninabox/lgsm-csgo 

To look inside of a running container:

    docker exec -ti <container-id> /bin/bash

## More Information

https://gameservermanagers.com/lgsm/csgoserver/
